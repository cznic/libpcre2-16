// Copyright 2023 The libpcre2-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libpcre2_16 is a ccgo/v4 version of libpcre2-16.a (https://www.pcre.org)
package libpcre2_16 // import "modernc.org/libpcre2-16"
